import random

from pid import PID
import time


def test_pid(P=0.2, I=0.0, D=0.0, L=100):
    """Self-test PID class
    .. note::
        ...
        for i in range(1, END):
            pid.update(feedback)
            output = pid.output
            if pid.SetPoint > 0:
                feedback += (output - (1/i))
            if i>9:
                pid.SetPoint = 1
            time.sleep(0.02)
        ---
    """
    pid = PID(P, I, D)

    pid.SetPoint = 0.0
    pid.set_sample_time(0.01)

    END = L
    feedback = 0

    for i in range(1, END):

        pid.update(feedback)

        if pid.SetPoint > 0:
            feedback += pid.output

        if i % 20 == 0:
            pid.SetPoint = random.randint(1, 10)
        time.sleep(0.02)

        print "{}: Setpoint: {}, Output: {}, error: {}".format(i, pid.SetPoint, pid.output, pid.SetPoint - feedback)


if __name__ == "__main__":
    test_pid(1.2, 1, 0.001, L=200)
