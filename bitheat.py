# coding=utf-8
"""
HOST: home.harrisons.pw:7472
USER: pi
PWD: bitHeatInsert01

"""
import math
import os
import pprint
import time
import datetime
import pymongo
from ads8201.ads8201 import ADS8201
import ssl
import wiringpi
from pid.pid import PID


# ---------------------  Constant values to be changed ----------------------------

# Reference Voltage
V_REF = 4.53
V_DD = 5.0185

# Chip names for each channels
ADC_CHANNELS_1 = ['LMT88'] * 8
ADC_CHANNELS_2 = ['AUX1', 'AUX2', 'LMT88', 'LMT88', 'KP22', 'MPXH', 'ACS', 'VIN']

# PWM channels
TEMP_ADC = 'ADC1'               # ADC number of the target temperature sensor, should be `ADC1` or `ADC2`
TEMP_CH = 3                     # Channel number of the target temperature sensor
TEMP_SETPOINT = 25              # Temperature set point
TEMP_FACTOR = 10                # Scala factor of temperature

PRESSURE_ADC = 'ADC2'           # ADC number of the target pressure sensor, should be `ADC1` or `ADC2`
PRESSURE_CH = 4                 # Channel number of the target pressure sensor
PRESSURE_SETPOINT = 100         # Pressure set point
PRESSURE_FACTOR = 10            # Scala factor of Pressure

WATT_SETPOINT = 25

# PID parameters
k_p = 1.2
k_i = 1
k_d = 0.01

# Constant of PWM channel
PWM_PIN = 18                    # We will use GPIO18 for PWM output.   NOTE: only a pin (BCM_GPIO 18) supports PWM
PWM_DIVIDER = 4                 # Set PWM frequency as 4.8kHz, see `README` for more detail.

# MongoDB
MONGODB_URL = 'aws-us-east-1-portal.21.dblayer.com'
MONGODB_PORT = 10783
MONGODB_DB = 'bh'
MONGODB_USER = 'wes'
MONGODB_PWD = 'nlnaew92y1mkfp9mAf35HyS'
MONGODB_COLLECTION = 'bitheat'


class ADCLogger:

    adc1 = None
    adc2 = None
    mongo_db = None

    duty_cycle = 0

    pid_temp = None
    pid_pressure = None
    pid_watt = None

    def __init__(self):
        self.adc1 = ADS8201(bus=0, device=1, v_ref=V_REF)
        self.adc2 = ADS8201(bus=0, device=0, v_ref=V_REF)
        self.initialize_mongodb()
        initialize_pwm()

        self.pid_temp = PID(p=k_p, i=k_i, d=k_d)
        self.pid_temp.SetPoint = TEMP_SETPOINT

        self.pid_pressure = PID(p=k_p, i=k_i, d=k_d)
        self.pid_pressure.SetPoint = PRESSURE_SETPOINT

        self.pid_watt = PID(p=k_p, i=k_i, d=k_d)
        self.pid_watt.SetPoint = WATT_SETPOINT

    def initialize_mongodb(self):
        """
        Initiliaze MongoDB connection
        :return:
        """
        client = pymongo.MongoClient(host=MONGODB_URL, port=MONGODB_PORT, ssl=True, ssl_cert_reqs=ssl.CERT_NONE)
        self.mongo_db = client.get_database(MONGODB_DB)
        print 'Authenticating mongodb...'
        if self.mongo_db.authenticate(name=MONGODB_USER, password=MONGODB_PWD, mechanism='SCRAM-SHA-1'):
            print 'Successfully authenticated to the remote MongoDB'
            if MONGODB_COLLECTION not in self.mongo_db.collection_names():
                self.mongo_db.create_collection(MONGODB_COLLECTION)
                print 'Created new collection named {}'.format(MONGODB_COLLECTION)
            return True
        else:
            print 'MongoDB authentication failed, please try again...'
            return False

    def read_all(self):
        """
        Read all values and convert to human-readable values
        :return:
        """
        r = dict()
        r['ADC1'] = dict()
        r['ADC2'] = dict()
        vin = current = 0
        self.adc1.open()
        print ' ------ ADC1 ------'
        for ch in range(8):
            volt = self.adc1.read_adc_val(ch)
            print 'CH {} Volt: {}'.format(ch, volt)
            if ADC_CHANNELS_1[ch] == 'LMT88':
                r['ADC1']['CH {} ({})'.format(ch, ADC_CHANNELS_1[ch])] = convert_lmt(volt)
            elif ADC_CHANNELS_1[ch] == 'KP22':
                r['ADC1']['CH {} ({})'.format(ch, ADC_CHANNELS_1[ch])] = convert_kp22(volt)
            elif ADC_CHANNELS_1[ch] == 'MPXH':
                r['ADC1']['CH {} ({})'.format(ch, ADC_CHANNELS_1[ch])] = convert_mpx(volt)
            elif ADC_CHANNELS_1[ch] == 'ACS':
                current = convert_acs(volt)
                r['ADC1']['CH {} ({})'.format(ch, ADC_CHANNELS_1[ch])] = current
            elif ADC_CHANNELS_1[ch] == 'VIN':
                vin = convert_divider(volt)
                r['ADC2']['CH {} ({})'.format(ch, ADC_CHANNELS_1[ch])] = vin
            else:
                r['ADC1']['CH {} ({})'.format(ch, ADC_CHANNELS_1[ch])] = volt

        if 'ACS' in ADC_CHANNELS_1 and 'VIN' in ADC_CHANNELS_1:
            r['WATTAGE'] = round(vin * current, 4)

        self.adc1.close()

        print ' ------ ADC2 ------'
        self.adc2.open()
        for ch in range(8):
            volt = self.adc2.read_adc_val(ch)
            print 'CH {} Volt: {}'.format(ch, volt)
            if ADC_CHANNELS_2[ch] == 'LMT88':
                r['ADC2']['CH {} ({})'.format(ch, ADC_CHANNELS_2[ch])] = convert_lmt(volt)
            elif ADC_CHANNELS_2[ch] == 'KP22':
                r['ADC2']['CH {} ({})'.format(ch, ADC_CHANNELS_2[ch])] = convert_kp22(volt)
            elif ADC_CHANNELS_2[ch] == 'MPXH':
                r['ADC2']['CH {} ({})'.format(ch, ADC_CHANNELS_2[ch])] = convert_mpx(volt)
            elif ADC_CHANNELS_2[ch] == 'ACS':
                current = convert_acs(volt)
                r['ADC2']['CH {} ({})'.format(ch, ADC_CHANNELS_2[ch])] = current
            elif ADC_CHANNELS_2[ch] == 'VIN':
                vin = convert_divider(volt)
                r['ADC2']['CH {} ({})'.format(ch, ADC_CHANNELS_2[ch])] = vin
            else:
                r['ADC2']['CH {} ({})'.format(ch, ADC_CHANNELS_2[ch])] = volt

        if 'ACS' in ADC_CHANNELS_2 and 'VIN' in ADC_CHANNELS_2:
            r['WATTAGE'] = round(vin * current, 4)

        self.adc2.close()
        return r

    def upload_mongodb(self, data):
        """
        Upload read data to mongodb
        :param data:
        :return:
        """
        data['datetime'] = datetime.datetime.now().strftime("%m/%d/%Y %H:%M:%S")
        data['epoch_time'] = int(time.time())
        collection = self.mongo_db.get_collection(MONGODB_COLLECTION)
        return collection.insert_one(data)

    def drive_pwm(self, data):
        """
        Drive PWM output
        :param data:
        :return:
        """
        target_temp = data[TEMP_ADC]['CH {} (LMT88)'.format(TEMP_CH)]
        self.pid_temp.update(target_temp)

        try:
            target_pressure = data[PRESSURE_ADC]['CH {} (KP22)'.format(PRESSURE_CH)]
        except KeyError:
            target_pressure = data[PRESSURE_ADC]['CH {} (MPXH)'.format(PRESSURE_CH)]

        self.pid_pressure.update(target_pressure)

        self.pid_watt.update(data['WATTAGE'])

        pid_output = self.pid_temp.output * TEMP_FACTOR
        # error = self.pid_pressure.output * PRESSURE_FACTOR
        # error = self.pid_watt.output * 10

        print 'PID output: {}'.format(pid_output)

        self.duty_cycle += int(pid_output)

        if self.duty_cycle < 0:
            self.duty_cycle = 0
        elif self.duty_cycle >= 1024:
            self.duty_cycle = 1023

        print 'Duty Cycle: {}'.format(self.duty_cycle)

        wiringpi.pwmWrite(PWM_PIN, self.duty_cycle)


def initialize_pwm():
    """
    Initialize PWM output.
    :return:
    """
    wiringpi.wiringPiSetupGpio()
    wiringpi.pinMode(PWM_PIN, 2)
    wiringpi.pwmSetClock(PWM_DIVIDER)
    # This function must be called after `pwmSetClock` function
    # I had to use my oscilloscope to find this, and it took about couple of hours to find this!
    wiringpi.pwmSetMode(wiringpi.PWM_MODE_MS)
    wiringpi.pwmWrite(PWM_PIN, 0)


def convert_lmt(v):
    """
    Convert voltage value to temperature value
    Formula:    V = -3.88*10^(-6) * T * T - 1.15 * 10^(-2) * T + 1.8639
                T = -1481.958 + (1.3225*10^(-4) + 4*3.88*10^(-6)*(1.8639-V))^0.5 / 3.88 * 1000000
    :return:
    """
    tmp = math.sqrt(1.3225 / 10000.0 + 4 * 3.88 * (1.8639 - v) / 1000000.0)
    t = -1481.958762 + tmp / 3.88 * 1000000 / 2.0
    # return "%.4f °C" % t
    return round(t, 4)


def convert_kp22(v):
    """
    Convert voltage value of KP229E3111 to pressure value
    Formula :   V_out = V_dd * (a * P + b)
                P = (V_out / V_dd - b) / a
        In datasheet, a = 0.00304, b = 0.01929
        We need calibration for accurate value of a & b
    :param v:
    :return:
    """
    a = 0.00304
    b = 0.01929
    # return "%.4f kPa" % ((v / 5 - b) / a)
    return round((v / 5 - b) / a, 4)


def convert_mpx(v):
    """
    Convert voltage value of MPXH6300 to pressure value
    Formula:    V_out = Vs x (0.00318 x P - 0.00353)
                P = (V_out / Vs + 0.00353) / 0.00318
    :param v:
    :return:
    """
    a = 0.00318
    b = 0.00353
    # return "%.4f kPa" % ((v / 5 + b) / a)
    return round((v / 5 + b) / a, 4)


def convert_acs(v, offset=2.5, scale_factor=0.04):
    """
    Convert voltage value of ACS758 to Current value
    Formula:    I = (V - ACOffset) / mVPerAmp
                    ACOffSet(offset):
                                bi-directional: VCC x 0.5
                                uni-directional: VCC x 0.12
                    mVPerAmp(scale_factor):
                                50A bi-directional: 0.04
                                50A uni-directional: 0.06
                                100A bi-directional: 0.02
                                100A uni-directional: 0.04
                                150A bi-directional: 0.0133
                                150A uni-directional: 0.0267
                                200A bi-directional: 0.01
                                200A uni-directional: 0.02
    http://henrysbench.capnfatz.com/henrys-bench/arduino-current-measurements/acs758-arduino-current-sensor-tutorial/
    :param scale_factor:
    :param offset:
    :param v:   Input voltage in mV
    :return:
    """
    # print 'Offset: {}, scale_factor: {}'.format(offset, scale_factor)
    # Sensitivity(scale_factor) is ratio-metric to the input voltage.
    # http://www.re-innovation.co.uk/web12/index.php/en/blog-2/361-acs756-current-measurements
    scale_factor = scale_factor * offset / (V_DD / 2.0)
    # return "%.4f A" % ((v - offset) / scale_factor)
    return round((v - offset) / scale_factor, 4)


def convert_divider(volt):
    """
    Voltage divider conversion
    I used a 1.2k and 680R resistor divider
    :param volt:
    :return:
    """
    return round(volt * 47.0 / 17.0, 4)


if __name__ == '__main__':

    if os.geteuid() != 0:
        exit("You need to have root privileges to run this script\nPlease try again with `sudo`\nExiting...")

    cc = ADCLogger()

    while True:
        s_time = time.time()
        result = cc.read_all()

        pprint.pprint(result)

        if cc.upload_mongodb(result):
            print 'Successfully uploaded to mongoDB'

        cc.drive_pwm(result)

        while time.time() - s_time < 60:
            time.sleep(.1)

    # # Read all register values and print
    # for i in range(11):
    #     print "Register {}: {}".format(i, ctrl.read_register(i))
    #
    # for i in range(8):
    #     print 'Channel {} reading: {}'.format(i, ctrl.read_adc_val(i))
