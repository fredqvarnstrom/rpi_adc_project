import time
from ads8201 import ADS8201


if __name__ == '__main__':
    a = ADS8201(bus=0, device=0)
    success = fail = 0
    for i in range(10000):
        if a.test_ads():
            success += 1
        else:
            fail += 1
        time.sleep(.001)

    print 'Test Result:'
    print 'Success: {}, Fail: {}'.format(success, fail)
