"""
Sophisticated class to control ADS8201 chip.
http://www.ti.com/product/ADS8201

"""
import random
import math
import time
from spidev import SpiDev
import RPi.GPIO as gpio


# Register Map
REG_CHANNEL_CCR = [0x00, 0x01, 0x02, 0x03]
REG_CHANNEL_SELECT_CCR = 0x04
REG_ADC_SCR = 0x05
REG_INTERRUPT_SCR = 0x06
REG_STATUS_SCR = 0x07
REG_ADC_TRIGGER_SCR = 0x08
REG_RESET_SCR = 0x09
REG_CONV_DELAY_SCR = 0x0A

spi = SpiDev()


class ADS8201:

    RST_PIN = 23
    CONVST_PIN = 24
    BUSY_PIN = 25
    BUSY_INT = True
    spi_bus = 0
    spi_device = 0
    max_speed = 15600000
    spi_mode = 1
    b_opened = False
    v_ref = 5.0
    disable_convst = False

    def __init__(self, bus=0, device=0, max_speed=7800000, rst_pin=23, convst_pin=24, busy_pin=25, mode=1,
                 busy_int=True, v_ref=5.0, disable_convst=True):
        """
        Constructor of this class
        :param bus: SPI bus number
        :param device: SPI device number
        :param max_speed: Max Clock Speed of SPI bus, ADS8201 has max speed of 25MHz.
            But I set its default value as 7.8MHz.
            For more detail, refer "SPEED" section of the link below:
                https://www.raspberrypi.org/documentation/hardware/raspberrypi/spi/README.md
        :param rst_pin: RST Pin Number in BCM format
        :param convst_pin: CONVST Pin Number in BCM format
        :param busy_pin: BUSY Pin number in BCM format
        :param mode: SPI mode, see `set_mode` for detail
        :param busy_int: BUSY/INT select mode. If True, BUSY/INT pin will be used as BUSY pin. (Datasheet 15p)
        :param v_ref: VDD voltage
        :param disable_convst: Read ADC value without CONVST pin or not.
        """
        self.disable_convst = disable_convst
        self.v_ref = v_ref
        self.spi_bus = bus
        self.spi_device = device
        self.max_speed = max_speed
        self.spi_mode = mode
        self.RST_PIN = rst_pin
        self.CONVST_PIN = convst_pin
        self.BUSY_PIN = busy_pin
        self.BUSY_INT = busy_int
        self.initialize_gpio()

    def open(self):
        """
        Open SPI bus and initialize ADS8201
        :return:
        """
        spi.open(self.spi_bus, self.spi_device)
        spi.max_speed_hz = self.max_speed
        self.set_mode(mode=self.spi_mode)                    # ADS8201 uses mode 1
        spi.lsbfirst = False                   # ADS8201 uses MSB first type.
        self.b_opened = True
        self.initialize_adc()

    def initialize_adc(self):
        """
        Initialize ADS8201
        :return:
        """
        # We will use `Manual trigger with manual channel update` mode
        self.write_register(REG_ADC_TRIGGER_SCR, 0x02)

        # Use BUSY/INT pin as BUSY or INT
        if self.BUSY_INT:
            # Set D[2] of ADC SCR as '1'
            val = self.read_register(REG_ADC_SCR)
            self.write_register(REG_ADC_SCR, val | 0x04)
        else:
            # Set D[2] of ADC SCR as '0'
            val = self.read_register(REG_ADC_SCR)
            self.write_register(REG_ADC_SCR, val & 0xFB)

        # print "Disable CONVST pin?"
        val = self.read_register(REG_ADC_SCR)
        if self.disable_convst:
            self.write_register(REG_ADC_SCR, val | 0x01)
        else:
            self.write_register(REG_ADC_SCR, val & 0xFE)

    def initialize_gpio(self):
        """
        Initialize GPIO of RPi
        :return:
        """
        gpio.setwarnings(False)
        gpio.setmode(gpio.BCM)
        gpio.setup(self.RST_PIN, gpio.OUT)
        gpio.setup(self.CONVST_PIN, gpio.OUT)
        gpio.setup(self.BUSY_PIN, gpio.IN)

        gpio.output(self.RST_PIN, True)
        gpio.output(self.CONVST_PIN, True)
        return True

    def test_ads(self):
        """
        Test connected ADS8201
        :return:
        """
        # Write new channel number and read back
        new_channel = random.randint(0, 7)
        print 'Setting new channel({}) for test'.format(new_channel)
        self.write_register(REG_CHANNEL_SELECT_CCR, new_channel)
        reg_val = self.read_register(REG_CHANNEL_SELECT_CCR)
        if new_channel == reg_val:
            return True
        else:
            print 'Failed to test ADS8201, expected: {}, received: {}'.format(new_channel, reg_val)
            return False

    def close(self):
        spi.close()
        self.b_opened = False
        return True

    @staticmethod
    def set_mode(mode):
        """
        Set SPI mode which controls clock polarity and phase.
        Should be a numeric value 0, 1, 2, or 3.  See wikipedia page for details on meaning:
        http://en.wikipedia.org/wiki/Serial_Peripheral_Interface_Bus
        :return:
        """
        if mode < 0 or mode > 3:
            raise ValueError('Mode must be a value 0, 1, 2, or 3.')
        spi.mode = mode

    def set_gain(self, channel, gain):
        """
        Set gain of channel
        :param channel:
        :param gain:
        :return:
        """
        if not self.b_opened:
            self.open()

        if gain not in [1, 2, 4, 8]:
            print 'Error, gain must be 1, 2, 4, 8'
            return False
        if not 0 <= channel < 8:
            print 'Error, channel must be 0 ~ 7'
            return False

        # Replace corresponding register value, see p.21 of datasheet
        channel_reg_addr = REG_CHANNEL_CCR[channel/2]
        offset = 0 if channel % 2 == 0 else 4
        reg_val = self.read_register(channel_reg_addr)
        new_val = ((reg_val >> (offset+2)) << (offset+2)) ^ (int(math.log(gain, 2)) << offset) ^ \
                  ((reg_val << (8-offset) & 0xff) >> (8 - offset))
        self.write_register(channel_reg_addr, new_val)

    def reset_adc(self):
        """
        Send software reset command
        :return:
        """
        self.write_register(REG_RESET_SCR, 0xAA)

    def set_average(self, avg_type, avg_num):
        """
        Set average value
        :param avg_type: 'fast', 'accurate', 'none'
        :param avg_num:  4, 8, 16
        :return:
        """
        if avg_type == 'none':
            new_val = 0
        else:
            if avg_num not in [4, 8, 16]:
                print 'Average number should be 4, 8, 16'
                return False
            # Get new val, see `ADC SCR` register for more detail
            try:
                new_val = ['fast', 'accurate'].index(avg_type) * 4 + [4, 8, 16].index(avg_num) + 1
            except ValueError:
                print 'Average type must be `fast`, `accurate`'
                return False
        old_val = self.read_register(REG_ADC_SCR)
        self.write_register(REG_ADC_SCR, (old_val & 0b00011111) | (new_val << 5))

    def read_register(self, address):
        """
        Read register value
             SDIN   | 0  | 1  | A3 | A2 | A1 | A0 | X  | X  || X  | X  | X  | X  | X  | X  | X  | X  |
             SDOUT  | X  | X  | X  | 0  | 0  | 0  | 0  | 0  || D7 | D6 | D5 | D4 | D3 | D2 | D1 | D0 |
        :param address: Register address
        :return:
        """
        if not self.b_opened:
            self.open()

        # Build a single channel read command.
        command = 0b01 << 6                  # Start bit, register read
        command |= (address & 0x0F) << 2
        resp = spi.xfer([command, 0, 0])
        # print 'Response: ', ' '.join(['{0:08b}'.format(r) for r in resp])
        return int(resp[1])

    def write_register(self, address, value):
        """
             SDIN   | 1  | 0  | A3 | A2 | A1 | A0 | X  | X  || D7 | D6 | D5 | D4 | D3 | D2 | D1 | D0 |
             SDOUT  | X  | X  | X  | 0  | 0  | 0  | 0  | 0  || X  | X  | X  | X  | X  | X  | X  | X  |
        :param address: Register address
        :param value: New register value, must in format of 1-byte
        :return:
        """
        if not self.b_opened:
            self.open()

        command = 0b10 << 6         # Start bit, register write
        command |= (address & 0x0F) << 2
        resp = spi.xfer([command, value])
        return ' '.join([hex(b) for b in resp])

    def read_adc_val(self, channel_num=0):
        """
            SDIN   |  0  | 0   |  X  |  X  |  X  |  X  | X  | X  || X | X  | X  | X  | X  | X  | X  | X  |
            SDOUT  | D15 | D14 | D13 | D12 | D11 | D10 | D9 | D8 | D7 | D6 | D5 | D4 | D3 | D2 | D1 | D0 |
            Read the current value of the specified ADC channel (0-7).
            The values can range from 0 to 4096 (12-bits).
        """
        # s_time = time.time()
        if not 0 <= channel_num <= 7:
            raise ValueError('ADC channel number must be a value of 0-7!')

        if not self.b_opened:
            self.open()

        # Switch channel
        self.write_register(REG_CHANNEL_SELECT_CCR, channel_num)
        # time.sleep(.001)

        # Set CONVST pin as low to start conversion
        if not self.disable_convst:
            gpio.output(self.CONVST_PIN, False)
            gpio.output(self.CONVST_PIN, True)

        # time.sleep(.1)
        # Wait for BUSY pin
        # if self.BUSY_INT:
        #     while GPIO.input(self.BUSY_PIN):
        #         time.sleep(.001)

        # Read ADC value of the channel
        command = 0x00

        # Read a value when CONVST pin is disabled
        # See page 23 of datasheet. It says "Ignore the first read"
        if self.disable_convst:
            spi.xfer([command, 0, 0])

        resp = spi.xfer([command, 0, 0])
        # We just need the first 12 bits, and others are average bits & TAG bits.   See page 16 of datasheet.
        # print 'Response: ', ' '.join(['{0:08b}'.format(r) for r in resp])
        result = ((resp[0] << 8) | resp[1]) >> 4
        # print 'Result value: {}'.format(result)
        # print 'Elapsed: ', time.time() - s_time
        return round(result / 4096.0 * self.v_ref, 4)


if __name__ == '__main__':

    print '----- ADC 1 -----'
    ctrl = ADS8201(bus=0, device=1, v_ref=4.53)
    ctrl.open()
    for i in range(8):
        print 'CH {}: {}V'.format(i, ctrl.read_adc_val(i))
    # print ' --- Reading all registers --- '
    for i in range(11):
        print 'REG {}: {}'.format(i, ctrl.read_register(i))
    ctrl.close()

    print '\n----- ADC 2 -----'
    a = ADS8201(bus=0, device=0, v_ref=4.53)
    a.open()

    for i in range(8):
        print 'CH ', i, ': ', a.read_adc_val(i), 'V'
    print

    for i in range(11):
        print 'REG {}: {}'.format(i, a.read_register(i))
    a.close()
