## Data logging system with various analog sensors and ADC(TI's ADS8201) chip.
 

### Connection Guide

![Raspberry Pi Pinout](Images/RP2_Pinout.png "Raspberry Pi Pinout")

- Raspberry Pi & ADS8201-1
    
    | **RPi**               | **ADS8201-1** |
    | :----:                |   :----:      |
    |  GND                  |    GND        | 
    |  GPIO11 (SPI0_SCLK)   |    SCLK (7)   |
    |  GPIO9  (SPI0_MISO)   |    SDO  (10)  |
    |  GPIO10 (SPI0_MOSI)   |    SDI  (9)   |
    |  GPIO7  (SPI0_CE1)    |    CS   (8)   |
    |  GPIO18               |    PWM        |
    
- Raspberry Pi & ADS8201-2
    
    | **RPi**               | **ADS8201-2** |
    | :----:                |   :----:      |
    |  GND                  |    GND        | 
    |  GPIO11 (SPI0_SCLK)   |    SCLK (7)   |
    |  GPIO9  (SPI0_MISO)   |    SDO  (10)  |
    |  GPIO10 (SPI0_MOSI)   |    SDI  (9)   |
    |  GPIO8  (SPI0_CE0)    |    CS   (8)   |

    
### Preparing Raspberry Pi.

- Expand file system.
    
        sudo raspi-config
        
    Select No.1 and expand file system, and reboot RPi.
    
- Update & upgrade packages.
    
        sudo apt-get update
        sudo apt-get upgrade

- Enable SPI0 bus on RPi.
    
        sudo raspi-config
    
    Go to `9(Advanced Options) -> A6(SPI)` and enable SPI bus, and reboot.
    
    
### Install dependencies

After enabling SPI bus, we need to install some python packages.
    
    sudo pip install spidev
    sudo apt-get install libpython-dev
    sudo pip install pymongo
    sudo pip install wiringpi

Sometimes Raspberry Pi fails to find DSN server.

To resolve this, we need to remove `dhcpcd5` package first.

    sudo apt-get purge dhcpcd5

Then add something in the file below:

    sudo nano /etc/resolv.conf
    
And add this:
    
    nameserver 208.67.222.222
    nameserver 208.67.220.220
    
### Choosing PWM frequency.

We will use `wiringpi` python library for PWM.

The `wiringpi` library has a base frequency of 19.2KHz in `mark:space` mode.

(See `pwmSetMode` function in https://projects.drogon.net/raspberry-pi/wiringpi/functions/ )
 
To use PWM, we need to divide this frequency.
    
If divider factor is 4, divided PWM frequency would be `19200 / 4800 = 4.8kHz`

NOTE:  Only a pin (BCM_GPIO 18) supports PWM.


    
    



    
